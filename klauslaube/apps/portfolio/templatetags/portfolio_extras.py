from django.template import Library
from portfolio.models import Job

register = Library()


@register.inclusion_tag('templatetags/job_list.html')
def job_list(limit=6):
    return {
        'jobs': Job.actives.all()[:limit]
    }
