from media_manager.utils import MediaTree
from media_manager.tests.base import MediaManagerBaseTests


class MediaTreeTests(MediaManagerBaseTests):
    def setUp(self):
        super(MediaTreeTests, self).setUp()
        self.tree = MediaTree(media_root=self.MEDIA_ROOT)

    def test_get_media_tree(self):
        self.assertEquals(self.tree.tree, self.paths)
        self.assertEquals(self.tree.urls, self.urls)

    def test_get_files_and_urls(self):
        self.assertEquals(self.tree.files, self.files)

    def test_get_file_infos(self):
        file_infos = self.tree.file_infos(url=self.tree.files[0]['url'])
        self.assertEquals(self.tree.files[0], file_infos)
