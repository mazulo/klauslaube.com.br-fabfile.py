from django.conf import settings
from django.test import SimpleTestCase
from django.test.client import RequestFactory
from core.context_processors import project_settings, project_versions


class CoreContextProcessorsTests(SimpleTestCase):
    def setUp(self):
        self.request = RequestFactory()

    def test_project_settings(self):
        """
        Should bring some settings values in a dict.
        """
        settings_dict = project_settings(self.request)
        self.assertEquals(settings_dict['SITE_NAME'], settings.SITE_NAME)

    def test_project_versions(self):
        """
        Should bring the version of the project principal apps.
        """
        versions_dict = project_versions(self.request)
        self.assertTrue(versions_dict.has_key('PYTHON_VERSION'))
        self.assertTrue(versions_dict.has_key('DJANGO_VERSION'))
        self.assertTrue(versions_dict.has_key('DIARIO_VERSION'))